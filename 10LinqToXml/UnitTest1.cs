﻿using System;
using System.Linq;
using System.Xml.Linq;
using NUnit.Framework;

namespace _10LinqToXml
{
    [TestFixture]
    public class LinqToXmlTests
    {
        [Test]
        public void Load()
        {
            var fromWeb = XDocument.Load("https://www.w3schools.com/xml/note.xml");

            Assert.That(fromWeb.Elements().Count(), Is.GreaterThanOrEqualTo(1));
        }

        [Test]
        public void Parse()
        {
            string xml = @"<customer id='123' status='archived'>

                 <firstname>Joe</firstname>

                 <lastname>Bloggs<!--nice name--></lastname>

               </customer>";

            var customer = XElement.Parse(xml);

            Assert.That(customer.Attribute("status").Value, Is.EqualTo("archived"));
        }

        [Test]
        public void CreationTest()
        {
            var lastName = new XElement("lastname", "Nowak");
            lastName.Add(new XComment("very popular surname"));
            var customer = new XElement("customer");
            customer.Add(new XAttribute("id", 123));
            customer.Add(new XElement("firstname", "Marcin"));
            customer.Add(lastName);

            var result = customer.ToString();

            Assert.That(result, Is.EqualTo(@"<customer id=""123"">
  <firstname>Marcin</firstname>
  <lastname>Nowak<!--very popular surname--></lastname>
</customer>"));
        }

        [Test]
        public void CreationTest2()
        {
            var customer =
                new XElement("customer", new XAttribute("id", "123"),
                    new XElement("firstname", "Marcin"),
                    new XElement("lastname", "Nowak", new XComment("very popular surname")
                    ));

            var result = customer.ToString();

            Assert.That(result, Is.EqualTo(@"<customer id=""123"">
  <firstname>Marcin</firstname>
  <lastname>Nowak<!--very popular surname--></lastname>
</customer>"));
        }

        [Test]
        public void CopyTest()
        {
            var address = new XElement("address",
                new XElement("street", "Klonowa"),
                new XElement("town", "Lublin"));

            var customer1 = new XElement("customer1", address);
            var customer2 = new XElement("customer2", address);

            customer1.Element("address").Element("street").Value = "Akacjowa";

            Assert.That(customer2.Element("address").Element("street").Value, Is.EqualTo("Akacjowa"));
        }
    }
}
