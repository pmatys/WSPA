﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    class AttributesBasic
    {
        private ObjectDifficultToCreate testedObj;

        [SetUp]
        public void SetUp()
        {
            //common procedure of test creation
            testedObj = new ObjectDifficultToCreate("param1", "param2", "param3");
        }

        [Test]
        public void Test()
        {
            var result = testedObj.TestedMethod();

            Assert.That(result, Is.EqualTo(3));
        }

        //Many test using testedObj

        [TearDown]
        public void TearDown()
        {
            //cleanUp
        }
    }

    class ValueAttributes
    {
        [Test]
        public void ValueTest(
            [Values(1, 2, 3)] int a,
            [Values(4, 5)] int b,
            [Values(6, 7)] int c)
        {
            var result = a * b * c;

            Assert.That(result, Is.GreaterThanOrEqualTo(24));
        }


        [Test, Pairwise]
        public void PairwiseTest(
            [Values(1, 2, 3)] int a,
            [Values(4, 5)] int b,
            [Values(6, 7)] int c)
        {
            var result = a * b * c;

            Assert.That(result, Is.GreaterThanOrEqualTo(24));
        }

        [Test, Combinatorial]
        public void CombinatorialTest(
            [Values(1, 2, 3)] int a,
            [Values(4, 5)] int b,
            [Values(6, 7)] int c)
        {
            var result = a * b * c;

            Assert.That(result, Is.GreaterThanOrEqualTo(24));
        }
    }

    class TestCaseAttributes
    {
        [TestCase(1, 2, 3)]
        [TestCase(1, 3, 4)]
        public void Test(int a, int b, int c)
        {
            var result = SomeFunc(a, b);

            Assert.That(result, Is.EqualTo(c));
        }


        [TestCase(1, 2, ExpectedResult = 3)]
        [TestCase(1, 3, ExpectedResult = 4)]
        public int ExpectedResult(int a, int b)
        {
            return SomeFunc(a, b);
        }

        private int SomeFunc(int a, int b)
        {
            return a + b;
        }
    }
    public class ObjectDifficultToCreate
    {
        private string[] manyParameters;

        public ObjectDifficultToCreate(params string[] manyParameters)
        {
            this.manyParameters = manyParameters;
        }

        public int TestedMethod()
        {
            return manyParameters.Length;
        }
    }
}
