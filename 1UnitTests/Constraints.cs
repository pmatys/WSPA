﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    class Constraints
    {
        [Test]
        public void AllItemsConstraint()
        {
            var range = Enumerable.Range(1, 10);

            Assert.That(range, Has.All.InstanceOf<int>());

            Assert.That(range, Has.All.GreaterThan(0));

            Assert.That(range, Has.None.GreaterThan(11));

            Assert.That(range, Has.Some.GreaterThan(9));

            Assert.That(range, Has.One.GreaterThanOrEqualTo(10));
        }

        [Test]
        public void AttributeExistsConstraint()
        {
            Assert.That(typeof(Fake), Has.Attribute<SerializableAttribute>());
        }

        [Test]
        public void CollectionContainsConstraint()
        {
            var array = new[] { 1, 2, 3 };

            Assert.That(array, Has.Member(2));
            Assert.That(array, Has.No.Member(5));
        }

        [Test]
        public void CollectionEquivalentConstraint()
        {
            var stringArray1 = new[] { "a", "b", "c" };
            var stringArray2 = new[] { "a", "b", "c", "d" };

            Assert.That(new[] { "c", "a", "b" }, Is.EquivalentTo(stringArray1));
            Assert.That(new[] { "c", "a", "b" }, Is.Not.EqualTo(stringArray1));

            Assert.That(stringArray1, Is.Not.EquivalentTo(stringArray2));
        }

        [Test]
        public void CollectionOrderedConstraint()
        {
            var array = new[] { "c", "b", "a" };
            Assert.That(array, Is.Ordered.Descending);
            Assert.That(array, Is.Not.Ordered.Ascending);

            var array2 = new[] { "aaa", "aa", "a" };
            Assert.That(array2, Is.Ordered.Descending.By("Length"));
        }

        [Test]
        public void CollectionSubsetConstraint()
        {
            var array = new[] { 1, 3 };
            Assert.That(array, Is.SubsetOf(new int[] { 1, 2, 3 }));
        }

        [Test]
        public void EmptyConstraint()
        {
            var list = new List<int>();
            list.Add(1);
          //  Assert.That(list, Is.Empty);
            Assert.That(new List<int>(), Is.Empty);
            Assert.That(new Dictionary<string, string>(), Is.Empty);
            Assert.That("", Is.Empty);
        }

        [Test]
        public void EndWithStartWith()
        {
            string phrase = "My favorite Test";

            Assert.That(phrase, Does.EndWith("test").IgnoreCase);
            Assert.That(phrase, Does.StartWith("My"));
        }

        [Test]
        public void ExactCountConstraint()
        {
            var array = new[] { 1, 1, 3 };

            Assert.That(array, Has.Exactly(2).Items.GreaterThan(1));
        }

        [Test]
        public void FalseTrueConstraint()
        {
            Assert.That(2 == 2, Is.True);
            Assert.That(2 != 2, Is.False);
        }

        [Test]
        public void InstanceOfTypeConstraint()
        {
            Assert.That(3, Is.InstanceOf<int>());
        }

        [Test]
        public void ComparisonConstraint()
        {
            Assert.That(5, Is.GreaterThan(4));
            Assert.That(5, Is.GreaterThanOrEqualTo(5));
            Assert.That(5, Is.Not.LessThan(4));
            Assert.That(5, Is.Not.LessThanOrEqualTo(4));
        }

        [Test]
        public void NaNConstraintAndNullConstraint()
        {
            var a = 0 / 0d;
            int? b = null;

            Assert.That(a, Is.NaN);
            Assert.That(b, Is.Null);
        }

        [Test]
        public void RangeConstraint()
        {
            var array = new[] { 1, 2, 3, 4 };

            Assert.That(array, Has.All.InRange(0, 4));
        }

        [Test]
        public void RegexConstraint()
        {
            var input1 = "1234a";
            var input2 = "a12";

            Assert.That(input1, Does.Match("^[0-9]+$"));           
            Assert.That(input2, Does.Not.Match("^[0-9]+$"));           
        }

        [Test]
        public void SubstringConstraint()
        {
            var str = "someString";

            Assert.That(str, Does.Contain("me"));
        }

        [Test]
        public void ThrowsConstraint()
        {
            Assert.That(Fake.ThrowTest, Throws.Exception.With.Message.EqualTo("myMessage"));
        }

        [Test]
        public void UniqueItemsConstraint()
        {
            var array1 = new[] { 1, 2, 3 };
            var array2 = new[] { 1, 1, 3 };

            Assert.That(array1, Is.Unique);
            Assert.That(array2, Is.Not.Unique);
        }

        [Test]
        public void AndOrConstraint()
        {
            var array = new[] { 1, 2, 11, 12 };

            Assert.That(array, Is.Unique.And.All.GreaterThan(0));

            Assert.That(array, Has.Some.GreaterThan(10).Or.LessThan(0));
        }

        [Test]
        public void EqualConstraint()
        {
            Assert.That(2 + 2, Is.EqualTo(4.0));
            Assert.That(5.01, Is.EqualTo(5).Within(1.5).Percent);

            Assert.That(2.1 + 1.2, Is.EqualTo(3.3));
            Assert.That(2.1 + 1.2, Is.EqualTo(3.3).Within(.0005));

            Assert.That("Hello!", Is.EqualTo("HELLO!").IgnoreCase);

            DateTime now = DateTime.Now;
            DateTime later = now + TimeSpan.FromHours(1.0);

            Assert.That(now, Is.EqualTo(now));
            Assert.That(later, Is.EqualTo(now).Within(TimeSpan.FromHours(3.0)));
            Assert.That(later, Is.EqualTo(now).Within(3).Hours);
        }
    }

    [Serializable]
    public class Fake
    {
        public static void ThrowTest()
        {
            throw new Exception("myMessage");
        }
    }
}
