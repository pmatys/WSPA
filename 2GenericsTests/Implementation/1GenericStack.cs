﻿namespace GenericsTests.Implementation
{
    class GenericStack<T>
    {
        int position;
        T[] data = new T[100];
        public void Push(T obj)
        {
            data[position++] = obj;
        }

        public T Pop()
        {
            return data[--position];
        }


        public string GetUsedType()
        {
            return typeof(T).ToString();
        }
    }
}
