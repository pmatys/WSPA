﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsTests.Implementation
{
    class ParametersTypeDeclared<T>
    {
        T Value { get; }
    }


    class ParametersTypeDeclared<T1, T2>
    {
        T1 Value1 { get; }
        T2 Value2 { get; }
    }
}
