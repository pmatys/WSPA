﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsTests.Implementation
{
    class BasicClass<T> where T : A
    {
        public void A(T a)
        {
            a.MyProperty = 5;
        }
    }

    class A
    {
        public int MyProperty { get; set; }
    }


    class B : A
    { }

    class C
    { }
}
