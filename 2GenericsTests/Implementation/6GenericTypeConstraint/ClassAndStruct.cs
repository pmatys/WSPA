﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsTests.Implementation.GenericTypeLimitation
{
    class ClassLimit<T> where T : class
    {
        //musy byc typu referencyjnego
    }

    class StructLimit<T> where T: struct
    {
        //musi być typem wartości
    }
}
