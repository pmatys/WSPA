﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsTests.Implementation.GenericTypeLimitation
{
    class ParemeterLessConstructor<T> where T : new()
    {
        public T[] CrateArray(int size)
        {
            var result = new T[size];
            for(int i=0; i<size; i++)
            {
                result[i] = new T();
            }

            return result;
        }
    }

    class WithParameterlessConstructor{
    }

    class WithoutParameterlessConstructor
    {
        public WithoutParameterlessConstructor(int x)
        {
        }
    }
}
