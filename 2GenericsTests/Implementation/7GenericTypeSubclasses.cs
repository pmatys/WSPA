﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsTests.Implementation
{
    class BaseType<T>
    {
    }

    class OpenSubClass<T> : BaseType<T>
    { }

    class CloseSubClass : BaseType<int>
    { }

    class EnhancedSubClass<T1, T2>: BaseType<T1>
    { }
}
