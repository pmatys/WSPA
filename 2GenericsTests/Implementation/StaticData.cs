﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsTests.Implementation
{
    class StaticDataOwner<T>
    {
        public static int StaticProperty { get; set; } = 0;
    }
}
