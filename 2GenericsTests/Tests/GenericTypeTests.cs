﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericsTests.Implementation;
using NUnit.Framework;

namespace GenericsTests.Tests
{
    [TestFixture]
    class GenericTypeTests
    {
        [Test]
        public void GenericMethodInNoGenericObject()
        {
            int x = 1;
            int y = 2;

            MyUtils.Swap(ref x, ref y);

            Assert.That(x, Is.EqualTo(2));
            Assert.That(y, Is.EqualTo(1));
        }

        [Test]
        public void DefaultValues()
        {
            var intArray = GenericArrayWithDefaulyValuesBuilder.Build<int>(2);
            var stringArray = GenericArrayWithDefaulyValuesBuilder.Build<string>(2);

            Assert.That(intArray[0], Is.EqualTo(0));
            Assert.That(stringArray[0], Is.EqualTo(null));
        }
    }
}
