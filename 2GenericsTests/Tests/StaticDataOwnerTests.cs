﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericsTests.Implementation;
using NUnit.Framework;

namespace GenericsTests.Tests
{
    [TestFixture]
    class StaticDataOwnerTests
    {
        [Test]
        public void StaticPropertyTests()
        {
            var sample1 = StaticDataOwner<int>.StaticProperty;
            StaticDataOwner<int>.StaticProperty++;
            var sample2 = StaticDataOwner<int>.StaticProperty;
            var sample3 = StaticDataOwner<double>.StaticProperty;

            Assert.That(sample1, Is.EqualTo(0));
            Assert.That(sample2, Is.EqualTo(1));
            Assert.That(sample3, Is.EqualTo(0));
        }

        [Test]
        public void T()
        {
            var a = new A();
            var b = new A();

            a.E = 3;
            a.SetS(4);

            Assert.That(b.E, Is.EqualTo(0));
            Assert.That(b.GetS(), Is.EqualTo(4));
        }
    }

    class A
    {
        public static int S;
        public int E;

        public void SetS(int s)
        {
            S = s;
        }

        public int GetS()
        {
            return S;
        }
    }
}
