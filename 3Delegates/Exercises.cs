﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using NUnit.Framework;
using Rhino.Mocks;

namespace _2Delegates
{
    [TestFixture]
    class Exercises1
    {

        //[Test]
        //public void GivenAList_WhenFindingEvenElement_ReturnsFirsOccuredElement()
        //{
        //    var array = new[] { 1, 2, 3, 4 };

        //    var result = Solution1.Filter(array, IsEven);

        //    Assert.That(result, Is.EqualTo(2));
        //}

        //[Test]
        //public void GivenAList_WhenFindingElementWithLengthGreaterOrEqual3_ReturnsFirsOccuredElement()
        //{
        //    var array = new[] { "a", "aaa", "bbb", "c" };

        //    var result = Solution1.Filter(array, ContainsMin3Sign);

        //    Assert.That(result, Is.EqualTo("aaa"));
        //}

        //[Test]
        //public void GivenAListContainingOnlyOddNumber_WhenFindingEvenElement_ExceptionIsThrown()
        //{
        //    var array = new[] { 1, 3, 5, 7 };

        //    Assert.That(() => Solution1.Filter(array, IsEven), Throws.Exception.With.Message.EqualTo("no element was found"));
        //}


        public bool IsEven(int x) => x % 2 == 0;
        public bool ContainsMin3Sign(string x) => x.Length >= 3;
    }

    [TestFixture]
    class Exercise2
    {
        [Test]
        public void GivenARestaurant_WhenAllTablesAreReserved_MangerIsInformed()
        {
            var manager = MockRepository.GenerateMock<IManager>();
            var tables = 2;
            var restaurant = new Restaurant(tables);
            restaurant.NoTableLeft += manager.CloseReservation;

            restaurant.ReserveTable();
            restaurant.ReserveTable();

            manager.AssertWasCalled(x => x.CloseReservation());
        }

        [Test]
        public void GivenARestaurantWithoutManager_WhenAllTablesAreReserved_NoExceptionIsThrown()
        {
            var manager = MockRepository.GenerateMock<IManager>();
            var tables = 2;
            var restaurant = new Restaurant(tables);

            restaurant.ReserveTable();
            restaurant.ReserveTable();
        }

    }
    public interface IManager
    {
        void CloseReservation();
    }

    public class Restaurant
    {
        public event Action NoTableLeft;

        private int tables;
        private int tablesLeft;

        public Restaurant(int tables)
        {
            this.tables = tables;
            this.tablesLeft = tables;
        }

        internal void ReserveTable()
        {
            tablesLeft--;
            if(tablesLeft == 0)
            {
                if (NoTableLeft != null)
                {
                    NoTableLeft();
                }
            }
        }
    }

}

