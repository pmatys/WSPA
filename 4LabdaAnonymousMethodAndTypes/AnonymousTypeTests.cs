﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace _3LabdaAnonymousMethodAndTypes
{
    [TestFixture]
    class AnonymousTypeTests
    {
        [Test]
        public void CreateAnonymousType()
        {
            var point = new { X = 3, Y = 4 };

            Assert.That(point.X, Is.EqualTo(3));
            Assert.That(point.GetType().ToString(), Is.EqualTo("<>f__AnonymousType0`2[System.Int32,System.Int32]"));
        }

        [Test]
        public void DeducedNames()
        {
            string Name = "John";

            var employee = new { Name, Name.Length, Age = 36 };

            Assert.That(employee.Name, Is.EqualTo(Name));
            Assert.That(employee.Length, Is.EqualTo(Name.Length));
        }

        [Test]
        public void DoesAnonymousTypeMatch()
        {
            var point1 = new { X = 13, Y = 14 };
            var point2 = new { X = 3, Y = 4 };

            var type = point1.GetType();
            var t1 = point1.GetType().ToString();
            var t2 = point2.GetType().ToString();

            Assert.AreEqual(point1.GetType(), point2.GetType());
        }

        [Test]
        public void AnonymousTypesTable()
        {
            var point1 = new { X = 13, Y = 14 };
            var point2 = new { X = 3, Y = 4 };

            var points = new[]
            {
                point1,
                point2
            };
        }
    }
}
