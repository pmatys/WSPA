﻿using System.Collections;
using NUnit.Framework;

namespace Collections.Collections
{
    [TestFixture]
    class BitArrayTests
    {
        //konstruktor
        [Test]
        public void GivenAnArray_WhenPassingArrayToListConstructor_ListIsCreated()
        {
            var array = new[] { true, false, true };

            var list = new BitArray(array);

            Assert.That(list.Count, Is.EqualTo(array.Length));
        }
    }
}
