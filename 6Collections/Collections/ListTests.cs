﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Collections.Collections
{
    [TestFixture]
    class ListTests
    {
        //konstruktor
        [Test]
        public void GivenAnArray_WhenPassingArrayToListConstructor_ListIsCreated()
        {
            var array = new[] { 1, 2, 3 };

            var list = new List<int>(array);

            Assert.That(list.Count, Is.EqualTo(array.Length));
        }
       

        //Add Elements
        [Test]
        public void GivenAnEnumerator_WhenInserting_ElementAreInsertedToList()
        {
            var list = new List<int>(new[] { 10, 11 });

            //list.Add(4);
            //list.Insert(1, 4);
            list.InsertRange(1, new[] { 4, 5 });

          //  list.InsertRange(1, GetEnumerator());

            Assert.That(list, Is.EqualTo(new[] { 10, 4,5,11 }));
            //Assert.That(list, Is.EqualTo(new[] { 10, 1, 2, 11 }));
        }

        private IEnumerable<int> GetEnumerator()
        {
            yield return 1;
            yield return 2;
        }
        //Remove Elements
        [Test]
        public void GivenAList_WhenRemoveingElements_ElementsAreRemoved()
        {
            var list = new List<int>(new[] { 1, 2, 3, 4, 5 });
            var firstRemoveResult = list.RemoveAll(x => x % 2 == 0);
            var secondRemoveResult = list.RemoveAll(x => x % 2 == 0);

            //Assert.That(firstRemoveResult, Is.EqualTo(2));
            //Assert.That(secondRemoveResult, Is.EqualTo(0));

            Assert.That(list, Is.EqualTo(new[] { 1, 3,5 }));
            //Assert.That(list, Is.EqualTo(new[] { 1, 3, 5 }));
        }

        //indeksowanie
        [Test]
        public void GivenAList_WhenGettingRange_RangeIsReturned()
        {
            var list = new List<int>(new[] { 1, 2, 3, 4, 5 });

            var result = list.GetRange(2, 2);

            Assert.That(result, Is.EqualTo(new[] { 3, 4 }));
        }


        //kopiowanie
        [Test]
        public void GivenAList_WhenCopy_ListIsCopied()
        {
            var list = new List<int>(new[] { 1, 2, 3, 4, 5 });
            var resultArray = new[] { 11, 4, 5, 6 };

            list.CopyTo(1, resultArray, 1, 3);

            Assert.That(resultArray, Is.EqualTo(new[] { 11, 2, 3, 4 }));
        }

        //pojemność
        [Test]
        public void GivenAList_CheckCapacity()
        {
            var list = new List<int>(new[] { 1, 2, 3, 4, 5 });
            Assert.That(list.Capacity, Is.EqualTo(5));

            TimeSpan result = AddManyElementToList(list);
        }

        private static TimeSpan AddManyElementToList(List<int> list)
        {
            var t1 = DateTime.UtcNow;
            for (int i = 0; i < 1000000; i++)
            {
                list.Add(i);
                //list.Insert(0, i);
            }
            var t2 = DateTime.UtcNow;

            var result = t2 - t1;
            return result;
        }

        [Test]
        public void GivenAList_SetCapacity()
        {
            var list = new List<int>(new[] { 1, 2, 3, 4, 5 });
            list.Capacity = 1000006;

            TimeSpan result = AddManyElementToList(list);
        }

    }
}
