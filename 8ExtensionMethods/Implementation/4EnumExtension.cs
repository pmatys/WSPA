﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9ExtensionMethods.Implementation
{
    public enum YesNo
    {
        Yes,
        No,
    }

    public static class EnumExtension
    {
        public static bool ToBool(this YesNo a)
        {
            if (a == YesNo.Yes) return true;
            return false;
        }

        public static YesNo ToYesNo(this bool a)
        {
            return a
                ? YesNo.Yes
                : YesNo.No;
        }

        public static YesNo ToYesNo(this bool? a)
        {
            return a==true
                ? YesNo.Yes
                : YesNo.No;
        }

    }
}
