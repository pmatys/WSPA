﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9ExtensionMethods.Implementation
{
    public class Base
    {
        public virtual string GetName()
        {
            return "Base";
        }
    }
    public class Derived : Base
    {
        public override string GetName()
        {
            return "Derived";
        }
    }
    public static class Extensions
    {
        public static string GetNameByExtension(this Base item)
        {
            return "Base";
        }
        public static string GetNameByExtension(this Derived item)
        {
            return "Derived";
        }
    }
}
