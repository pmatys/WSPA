﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9ExtensionMethods.Implementation
{
    public interface ITimeFormatter
    {
        string Format(TimeSpan span);
    }
    public static class TimeFormatter
    {
        public static string Format(
        this ITimeFormatter formatter,
        int millisecondsSpan)
        => formatter.Format(TimeSpan.FromMilliseconds(millisecondsSpan));
    }

    public class SecondsTimeFormatter : ITimeFormatter
    {
        public string Format(TimeSpan span)
        {
            return $"{(int)span.TotalSeconds}s";
        }
    }
}
