﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9ExtensionMethods.Implementation
{
    public static class DictExtensions
    {
        public static List<int> ComputeIfAbsent(this Dictionary<string, List<int>> dictList, string key,  Func<List<int>> action)
        {
            if (!dictList.Keys.Contains(key))
            {
                dictList[key] = action();
            }

            return dictList[key];
        }


        public static void Add(this Dictionary<string, List<int>> dictList, string key, int value, Func<List<int>> action =null )
        {
            if (!dictList.Keys.Contains(key))
            {
                action = action ?? Test;
                dictList[key] = action();
            }
            dictList[key].Add(value);
        }

        private static List<int> Test() { throw new Exception("now Key found"); }
    }
}



//łancuch add
//action if absent